MA GRIGORIO Advocacia Especializada tem como propósito exercer uma advocacia humanitária que prima pela excelência na prestação de serviços advocatícios, na busca incessante pelo melhor direito a ser aplicado.

Somos uma equipe unida e calcada na lealdade, honestidade e transparência. Nossa trajetória traz em nossa raiz a determinação e excelência em serviços advocatícios, visando sempre ao atendimento personalizado a cada cliente.

Empatia, ética, inovação, compromisso com o bom, o belo e o justo que faz esse escritório ser destaque na advocacia.

Missão
A nossa missão é imprimir excelência e humanidade aos serviços jurídicos prestados com eficiência, ética e sensibilidade ao inovar nas soluções jurídicas. Entendemos a advocacia como um instrumento de transformação e garantidor da função social das empresas e da dignidade humana.

Visão
Ser o escritório referência em âmbito nacional por proporcionar soluções inteligentes às questões jurídicas, por meio de estratégias inovadoras para indivíduos e empresas.

Valores
Empatia, Ética, Inovação, Integridade, Excelência nos serviços advocatícios e Compromisso com o bom, o belo e o justo.

Contato
MA GRIGORIO - Advogados Em Brasilia
Endereço:  SBS Quadra 02, Sala 206 Ed. Prime Business Convenience, Asa Sul, Brasília-DF
Telefone:  (61) 3041-9560
Whatsapp:  (61) 99687-2625
E-mail:  contato@magrigorioadvocacia.com.br
Website: https://magrigorioadvocacia.com.br/